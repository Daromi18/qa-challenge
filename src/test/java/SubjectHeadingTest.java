

import static org.junit.Assert.assertTrue;

import java.time.Clock;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qachallenge.contact_form.Config;
import com.qachallenge.contact_form.ContactStepDefinition;

import pageObjects.ContactPO;

public class SubjectHeadingTest {
	
	private WebDriver driver = Config.getChromeDriver();
	
	@Test
	public void testSubjectCustomerService(){
			ContactStepDefinition  base = new ContactStepDefinition(driver);
			
			String sDropdown = base.selectCustomerServiceOption();
			Assert.assertEquals("Customer service", sDropdown);
			Assert.assertNotNull(sDropdown);
		
	}
	
	@Test
	public void testSubjectWebMaster()  {
			ContactStepDefinition  base = new ContactStepDefinition(driver);
			
			String sDropdown = base.selectWebmasterOption();
			Assert.assertEquals("Webmaster", sDropdown);
			
	}
	
	@Test
	public void testEmail() {
			ContactStepDefinition base = new ContactStepDefinition(driver);
			
			String expectedEmail = "email@test.com";
			String actualEmail = base.selectEmail();
			Assert.assertEquals(actualEmail, expectedEmail);
			Assert.assertNotNull(actualEmail);
		
	}
	
	@Test
	public void testOrderReference() {
			ContactStepDefinition base = new ContactStepDefinition(driver);
	
			String actualReference = base.getOrderReference();
			String expectedReference = "TSR1234";
			Assert.assertEquals(actualReference, expectedReference);
			Assert.assertNotNull(actualReference);
	}
	
	@Test
	public void testMessageTextBox() {
			ContactStepDefinition base = new ContactStepDefinition(driver);
			
			String actualText = base.getTextMessage();
			Assert.assertNotNull(actualText);
	}
	
	@Test
	public void verifyTitle() {
			String actualTitle = driver.getTitle();
			String expectedTitle = "Contact us - My Store";
			Assert.assertEquals(actualTitle, expectedTitle);
	}
	
	@Test
	public void testSendMessage() {
			ContactStepDefinition base = new ContactStepDefinition(driver);
			ContactPO pObject = new ContactPO(driver);
			base.selectCustomerServiceOption();
			base.selectEmail();
			base.getOrderReference();
			base.getTextMessage();
			pObject.getSendButton().click();
			
	}
	
	@Before
	public void testTimer() throws InterruptedException {
			TimeUnit.SECONDS.sleep(3);
			System.out.println("Before method");
	}
	

	@After
	public void closeBrowser() {
		driver.quit();
	}

}
