package pageObjects;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactPO {
	
	private WebDriver driver;

	
	public ContactPO(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getSubject() {
		return driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/form/fieldset/div[1]/div[1]/div[1]/div/select"));
	}

	
	public WebElement getEmailField() {
		return driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/form/fieldset/div[1]/div[1]/p[4]/input"));
	}
	

	
	public WebElement getOrderReference() {
		return  driver.findElement(By.id("id_order"));
	}
	
	public  WebElement getChooseFileButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement chooseFile = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div[2]/div/div[3]/div/form/fieldset/div[1]/div[1]/p[5]/div/span[2]")));
		return chooseFile;
	}
	
	public WebElement getTextBox() {
		return driver.findElement(By.id("message"));
	}
	
	public WebElement getSendButton() {
		return driver.findElement(By.id("submitMessage"));
	}
	

}
