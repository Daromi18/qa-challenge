package com.qachallenge.contact_form;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import pageObjects.ContactPO;

public class ContactStepDefinition {
	
			private WebDriver driver;
			
		public ContactStepDefinition(WebDriver driver) {
				this.driver = driver;
			}
		
		public String selectCustomerServiceOption() {
			ContactPO sub = new ContactPO(driver);
			
			WebElement webEle = sub.getSubject();
			Select select = new Select(webEle);
			select.selectByIndex(1);
			
			String selectedDropdown = select.getFirstSelectedOption().getText();
			return selectedDropdown;
		}
		
		public String selectWebmasterOption() {
			ContactPO sub = new ContactPO(driver);
			
			WebElement webEle = sub.getSubject();
			Select select = new Select(webEle);
			select.selectByIndex(2);
			
			String selectedDropdown = select.getFirstSelectedOption().getText();
			return selectedDropdown;
		}
		
		public String selectEmail() {
			
			ContactPO sub = new ContactPO(driver);
			sub.getEmailField().sendKeys("email@test.com");
			String inbox = sub.getEmailField().getAttribute("value");
			return inbox;
		}
		
		public String getOrderReference() {
			ContactPO sub = new ContactPO(driver);
			sub.getOrderReference().sendKeys("TSR1234");
			
			String text = sub.getOrderReference().getAttribute("value");
			return text;
		}
		
		public String getTextMessage() {
			ContactPO sub = new ContactPO(driver);
			sub.getTextBox().sendKeys("Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
			String text = sub.getTextBox().getAttribute("value");
			return text;
		}

}
