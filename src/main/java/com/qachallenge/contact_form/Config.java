package com.qachallenge.contact_form;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Config {
	
	public static WebDriver getChromeDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\DRODR\\eclipse-workspace\\contact-form\\src\\main\\resources\\Driver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		System.out.println("Starting CHROME");
		driver.get("http://automationpractice.com/index.php?controller=contact");
		return driver;
	}

}
